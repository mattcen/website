## About

### FeralSec

FeralSec are four spooky pals based in Australia, who all care about privacy, security, empathy & anti-capitalism (amongst other special interests).

Some topics we find ourselves thinking about?

* How can we bring empathy and anti-authoritarian thinking to what we do?
* Which tools can be use to build more human processes for protecting user security and privacy?
* How can we build better representation for under-represented groups in our communities and industry?
* How can we share knowledge and tooling across out communities in a way that is interesting and fun?

### The Team

* attacus / Lilly (she/her)
* Bec / errbufferoverfl (they/them)
* Mimi (she/her)
* Rayne (they/them)

### Why FeralSec?

It sounds pretty cool, and we're all feral little gremlins, and we bring that energy to everything we do.

### FeralTV

Come join us when we hang out and chat about how we can all build good privacy, security, empathy and anti-capitalist skills into our work and our daily lives.

* [Twitch Live-stream](https://twitch.tv/feralsec)
* [YouTube Show Recordings](https://youtube.com/@FeralSec)




