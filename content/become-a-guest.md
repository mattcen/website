## Become a Guest

### Submit Your Talk

We're so delighted you want to come and be feral with us! Send us an email at [feralsec@protonmail.com](mailto:feralsec@protonmail.com) and we'll be in touch with next steps. 

### Getting Ready to Stream

We use [Streamyard](https://streamyard.com) to run the show. You'll receive a unique invite link and there's no sign-up required. Here are some basic tips on getting the best possible stream quality.

* Check the device, browser and connection you're streaming from meets [Streamyard's requirements](https://support.streamyard.com/hc/en-us/articles/360061299051-StreamYard-Requirements).
* When you enter the Streamyard studio, click the gear icon to check your audio and video settings. Bump the camera resolution up to 720p and check your preferred microphone is selected.
* You may need to give your browser permission to access your camera, microphone and screen sharing. Usually this requires a browser restart so test it out early to avoid an awkward re-join!
* Make sure you're evenly and well-lit. This [blog post](https://restream.io/blog/video-lighting/#lighting-schemes-for-indoor-video-production) by Restream has some great tips.
* Position your microphone well so we can hear you -- but not like a pre-teen on Xbox FPS games. This [blog post](https://www.buzzsprout.com/blog/mic-technique-podcasting) by Buzzsprout walks through the best way to set up your mic.
